/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  purge: ['./src/**/*.{js,jsx,ts,tsx,vue,html}'],
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [],
}
