import { trpc } from '@/utils/trpc'
import type { NextPage } from 'next'

const Home: NextPage = () => {
  const { data, error, isLoading } = trpc.useQuery(['hello', { text: "Quentin" }])

  if(isLoading) return <div>Loading...</div>

  if(error) return <div>{error.message}</div>

  return (
    <div className="h-screen w-screen flex flex-col justify-center items-center">
      <h1 className="text-2xl text-center">Welcome to the Profiler Island !</h1>
      <span>{data?.greeting}</span>
    </div>
  )
}

export default Home
